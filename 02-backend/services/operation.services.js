const boom = require('@hapi/boom');

const pool = require('../libs/postgres');

class OperationService {
  constructor() {
    this.pool = pool;
    this.pool.on('error', (error) => console.log(error));
  }

  /**
   * Service to obtain a list of operations
   * @param data given filters
   * @returns Array with list of operations || undefined
   */
  async getOperations(data) {
    let query;
    if (data.operation) {
      query = `SELECT * FROM "ABM_Operations" WHERE type_operation_id = ${data.operation}`;
    } else {
      query = 'SELECT * FROM "ABM_Operations"';
    }
    const types = await this.pool.query(query);
    return types.rows;
  }

  /**
   * Service to obtain a operator
   * @param operationId identification of the operator
   * @returns operator information
   */
  async getOperatorById(id) {
    const Query = 'SELECT * FROM "ABM_Operations" WHERE operation_id = $1';
    const Operator = await this.pool.query(Query, [id]);
    if (Operator.rows.length === 0) {
      throw boom.notFound('Operator not found');
    }
    return Operator.rows[0];
  }

  /**
   * service to create an operator
   * @param data information about operator to create
   * @returns new operator
   */
  async postOperator(data) {
    const Query = `INSERT INTO "ABM_Operations"
                    (type_operation_id, user_email, monto, concept, creation_date)
                    VALUES($1, $2, $3, $4, now())`;
    const NewOperator = await this.pool.query(Query, [
      data.typeOperationId,
      data.userId,
      data.monto,
      data.concept,
    ]);
    return NewOperator;
  }

  /**
   * service to edit a operator
   * @param data new information of operator
   * @param id identification of the operator
   * @returns new information of operator
   */
  async updateOperator(data, id) {
    const Query = `UPDATE "ABM_Operations" SET
    monto = $1, concept = $2
    WHERE operation_id = ${id}
    RETURNING operation_id`;
    const UpdateOperation = await this.pool.query(Query, [
      data.monto,
      data.concept,
    ]);
    return UpdateOperation;
  }

  /**
   * service to delete a operator
   * @param id identification of the operator
   */
  async deleteOperator(id) {
    const Query = `DELETE FROM "ABM_Operations"
    WHERE operation_id = ${id}`;
    const DeleteOperator = await this.pool.query(Query);
    return DeleteOperator;
  }

  /**
   * Service to obtain a list of types of operations
   * @returns Array with list of of types of operations
   */
  async getOperationsType() {
    const Query = 'SELECT * FROM "ABM_Types_Operation"';
    const types = await this.pool.query(Query);
    return types.rows;
  }
}
module.exports = OperationService;
