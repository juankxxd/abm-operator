const boom = require('@hapi/boom');
const pool = require('../libs/postgres');

class UserService {
  constructor() {
    this.pool = pool;
    this.pool.on('error', (error) => console.log(error));
  }

  /**
   * Service to obtain a user
   * @param data info of the user
   * @returns infor of the user || undefined
   */
  async Login(data) {
    const Query = 'SELECT * FROM "ABM_Users" WHERE user_email = $1 AND password_user = $2';
    const user = await this.pool.query(Query, [
      data.userEmail,
      data.userPassword,
    ]);

    if (user.rows.length === 0) {
      throw boom.notFound('invalid user data');
    }
    return user.rows[0];
  }

  /**
   * service to create an user
   * @param data information about user to create
   * @returns new user
   */
  async postUser(data) {
    const Query = `INSERT INTO "ABM_Users"
                    (user_email, user_name, password_user)
                    VALUES($1, $2, $3)`;
    const NewUser = await this.pool.query(Query, [
      data.userEmail,
      data.userName,
      data.userPassword,
    ]);
    return NewUser;
  }
}
module.exports = UserService;
