const express = require('express');
const cors = require('cors');
const { config } = require('./config/config');
const routerApi = require('./routes/index');
const {
  logErrors,
  errorHandler,
  boomErrorHandler,
} = require('./middlewares/error.handler');

const app = express();
const PORT = config.port;
const corsOptions = {
  origin: 'http://localhost:3000',
};
app.use(express.json());
app.use(cors(corsOptions));
routerApi(app);

app.use(logErrors);
app.use(boomErrorHandler);
app.use(errorHandler);
app.listen(PORT, (err) => {
  if (err) {
    console.error('Error listen: ', err);
    return;
  }
  // Si no se detuvo arriba con el return, entonces todo va bien ;)
  console.log(`Server is running on port ${PORT}`);
});
