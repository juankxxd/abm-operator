const express = require('express');

const router = express.Router();

// const passport = require('passport');
const OperationService = require('../services/operation.services');

const operationService = new OperationService();
const {
  getOperationsSchema,
  createOperationsSchema,
  updateOperationsmSchema,
} = require('../schemas/operations.schema');
const validatorHandler = require('../middlewares/validator.handler');

/**
 * Get all type of operations
 */
router.get('/type-operations', async (req, res, next) => {
  try {
    const response = await operationService.getOperationsType();
    res.status(200).json(response);
  } catch (error) {
    next(error);
  }
});

/**
 * Get all operators
 */
router.post('/operators', async (req, res, next) => {
  try {
    const response = await operationService.getOperations(req.body);
    res.status(200).json(response);
  } catch (error) {
    next(error);
  }
});

/**
 * Get one operator
 */
router.get(
  '/:operationId',
  validatorHandler(getOperationsSchema, 'params'),
  async (req, res, next) => {
    try {
      const OperatorId = parseInt(req.params.operationId, 10);
      const response = await operationService.getOperatorById(OperatorId);
      res.status(200).json(response);
    } catch (error) {
      next(error);
    }
  },
);

/**
 * Create a new operator
 */
router.post(
  '/create',
  validatorHandler(createOperationsSchema, 'body'),
  async (req, res, next) => {
    try {
      const Data = req.body;
      await operationService.postOperator(Data);
      res.status(201).json('successfully created');
    } catch (error) {
      const Err = {
        error: false,
        message: error.message,
      };
      res.status(400).json(Err);
      next(error);
    }
  },
);

/**
 * Update a operation
 */
router.put(
  '/edit/:operationId',
  validatorHandler(getOperationsSchema, 'params'),
  validatorHandler(updateOperationsmSchema, 'body'),
  async (req, res, next) => {
    try {
      const OperatorId = parseInt(req.params.operationId, 10);
      const response = await operationService.updateOperator(req.body, OperatorId);
      res.status(200).json(response);
    } catch (error) {
      next(error);
    }
  },
);

/**
 * Delete a operator
 */
router.delete(
  '/delete/:operationId',
  validatorHandler(getOperationsSchema, 'params'),
  async (req, res, next) => {
    try {
      const OperatorId = parseInt(req.params.operationId, 10);
      const response = await operationService.deleteOperator(OperatorId);
      res.status(200).json(response);
    } catch (error) {
      next(error);
    }
  },
);

module.exports = router;
