const express = require('express');

// import routes
const OperationRoute = require('./operation.router');
const UserRoute = require('./user.router');
// use all routes
function routerApi(app) {
  const router = express.Router();
  app.use('/api', router);
  router.use('/operations', OperationRoute);
  router.use('/user', UserRoute);
}

module.exports = routerApi;
