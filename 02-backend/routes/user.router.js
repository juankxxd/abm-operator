const express = require('express');

const router = express.Router();

// const passport = require('passport');
const UserService = require('../services/user.service');

const userService = new UserService();
const {
  createUserSchema,
} = require('../schemas/user.schema');
const validatorHandler = require('../middlewares/validator.handler');

/**
 * Login
 */
router.post('/login', async (req, res, next) => {
  try {
    const response = await userService.Login(req.body);
    res.status(200).json(response);
  } catch (error) {
    next(error);
  }
});

/**
 * Create a new operator
 */
router.post(
  '/create',
  validatorHandler(createUserSchema, 'body'),
  async (req, res, next) => {
    try {
      const Data = req.body;
      await userService.postUser(Data);
      res.status(201).json('successfully created');
    } catch (error) {
      const Err = {
        error: false,
        message: error.message,
      };
      res.status(400).json(Err);
      next(error);
    }
  },
);

module.exports = router;
