const boom = require('@hapi/boom');

/**
 * validate the shemas in the routes
 * @param {*} schema shema to validate the request body data or parameters
 * @param {*} property the validation type params or body
 */

function validatorHandler(schema, property) {
  return (req, res, next) => {
    const data = req[property];
    const { error } = schema.validate(data, { abortEarly: false });
    if (error) {
      res.status(400).json({
        error: 'false',
        message: error.message,
      });
      next(boom.badRequest(error));
    }
    next();
  };
}

module.exports = validatorHandler;
