const Joi = require('joi');

const userName = Joi.string();
const userEmail = Joi.string().email();
const userPassword = Joi.string();

/**
 * schema to validate input data
 */
const getUserSchema = Joi.object({
  userEmail: userEmail.required(),
  userPassword: userPassword.required(),
});

/**
 * validate data to create operation
 */
const createUserSchema = Joi.object({
  userName: userName.required(),
  userEmail: userEmail.required(),
  userPassword: userPassword.required(),
});

/**
 * export the validators
 */
module.exports = {
  createUserSchema,
  getUserSchema,
};
