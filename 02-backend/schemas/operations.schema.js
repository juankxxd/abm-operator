const Joi = require('joi');

const operationId = Joi.number().integer();
const userId = Joi.string();
const typeOperationId = Joi.number().integer();
const monto = Joi.number().integer().positive().allow(0);
const concept = Joi.string();

/**
 * schema to validate input data
 */
const getOperationsSchema = Joi.object({
  operationId: operationId.required(),
});
/**
 * validate data to create operation
 */
const createOperationsSchema = Joi.object({
  userId: userId.required(),
  typeOperationId: typeOperationId.required(),
  monto: monto.required(),
  concept: concept.required(),
});
/**
 * validate data to update an operation
 */
const updateOperationsmSchema = Joi.object({
  userId: userId.required(),
  monto: monto.required(),
  concept: concept.required(),
});

/**
 * export the validators
 */
module.exports = {
  getOperationsSchema,
  createOperationsSchema,
  updateOperationsmSchema,
};
