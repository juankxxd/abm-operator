CREATE TABLE "ABM_Types_Operation"
(
    type_operation_id SMALLSERIAL NOT NULL,
    description_type_operation CHARACTER VARYING(64) NOT NULL,

    PRIMARY KEY (type_operation_id)
);

INSERT INTO "ABM_Types_Operation"(description_type_operation) VALUES('Ingreso');
INSERT INTO "ABM_Types_Operation"(description_type_operation) VALUES('Egreso');