CREATE TABLE "ABM_Operations"
(
    operation_id SERIAL NOT NULL,
    user_email CHARACTER VARYING(64) NOT NULL,
    type_operation_id INTEGER NOT NULL,
    monto INTEGER NOT NULL,
    creation_date TIMESTAMP,
    concept CHARACTER VARYING(64) NOT NULL,

    PRIMARY KEY (operation_id),

    CONSTRAINT "FK_ABM_TYPES_OPERATION_TYPE_OPERATION_ID" FOREIGN KEY (type_operation_id)
        REFERENCES "ABM_Types_Operation" (type_operation_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID,

    CONSTRAINT "FK_ABM_USERS_USER_EMAIL" FOREIGN KEY (user_email)
        REFERENCES "ABM_Users" (user_email) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
);