import React, { useEffect } from 'react'
import { useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { selectUser } from '../features/userSlice';
export default function Home() {
  const { user } = useSelector(selectUser);
  const navigate = useNavigate();
  useEffect(() => {
    if (!user.isLogged) {
      navigate('/login');
    }   
  }, [])
  
  return (
    <div>Home</div>
  )
}
