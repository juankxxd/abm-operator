import React, { useEffect, useState } from 'react'
import Eye from '../assets/Eye';
import { useNavigate } from 'react-router-dom';
import '../styles/Login.css'
import { inputLabelUp, inputLabelDown } from '../tools/inputs'; 
import httpFetch from '../tools/httpFetch';
import { useDispatch, useSelector } from 'react-redux';
import { selectUser, login } from '../features/userSlice';

export default function Login() {
  const [userData, setUserData] = useState({password: '', email: ''});
  const [type, setType] = useState(false);
  const [error, setError] = useState(false);
  const api = httpFetch();
  const navigate = useNavigate();
  const { user } = useSelector(selectUser);
  const dispatch = useDispatch();

  useEffect(() => {
    if (user.isLogged) navigate('/');
  }, []);


  const handleInput = (e) => {
    userData[e.target.name] = e.target.value;
    setUserData({ ...userData });
  }

  const submitLogin = (e) => {
    e.preventDefault();
    api.post('/user/login', { body: { userPassword: userData.password, userEmail: userData.email } })
        .then((response) => {
          if (!response.err) {
            dispatch(login(response));
            navigate('/');
          } else {
            setError(true);
          }
          console.log(response);
        });
  }

  return (
    <div className='login'>
      <div className='login-container'>
      <h1>Log In</h1>
        <form onSubmit={submitLogin}>
          <div className='input-label'>
            <label htmlFor='email'>Email</label>
            <input name='email' value={userData.email} onChange={handleInput} onFocus={inputLabelUp}
            onBlur={inputLabelDown}/>
          </div>
          <div className='input-label'>
            <label htmlFor='password'>Password</label>
            <input type={type ? 'text' : 'password'} name='password' value={userData.password} onChange={handleInput}
            onFocus={inputLabelUp} onBlur={inputLabelDown}/>
            <Eye type={type} handlePassword={(e) => setType(!type)}/>
          </div>
          <div className='btns'>
          <input type='submit' value='Login'/>
          <button onClick={() => navigate('/register')}>Sign in</button>
          </div>
          {error && <span>invalid data</span>}
        </form>
      </div>
    </div>
  )
}
