import React, { useState } from 'react'
import { useNavigate } from 'react-router-dom';
import Eye from '../assets/Eye';
import httpFetch from '../tools/httpFetch';
import { inputLabelUp, inputLabelDown } from '../tools/inputs'; 
import '../styles/Register.css'

export default function Register() {
  const [user, setUser] = useState({password: '', email: '', name: ''});
  const [type, setType] = useState(false);
  const [message, setMessage] = useState();
  // const [typeOperation, setTypeOperation] = useState();
  const api = httpFetch();
  const navigate = useNavigate();

  // useEffect(() => {
  //   api.get('/operations/type-operations').then((data) => {
  //     setTypeOperation(data)
  //     console.log(typeOperation)
  //   })
  // }, [])
  

  const handleInput = (e) => {
    console.log('entro aqui?');
    user[e.target.name] = e.target.value;
    setUser({ ...user });
  }

  const submitLogin = (e) => {
    console.log('entro aqui')
    e.preventDefault();
    if(!user.name) {
      setMessage('You must add a name');
    } else if(!user.email){
      setMessage('You must add a email');
    } else if(!user.password){
      setMessage('You must add a password');
    } else {
      api.post('/user/create', { body: { userPassword: user.password, userEmail: user.email, userName: user.name } })
        .then((response) => {
          if (!response.err) {
            navigate('/');
          } else {
            setMessage('try again later');
          }
          console.log(response);
        });
    }
  }


  return (
    <div className='register                       '>
      <div className='register-container'>
        <h1>Sign up</h1>
        <form onSubmit={submitLogin}>
          <div className='input-label'>
            <label htmlFor='name'>name</label>
            <input name='name' value={user.name} onChange={handleInput} onFocus={inputLabelUp}
            onBlur={inputLabelDown}/>
          </div>
          <div className='input-label'>
            <label htmlFor='email'>Email</label>
            <input name='email' value={user.email} onChange={handleInput} onFocus={inputLabelUp}
            onBlur={inputLabelDown}/>
          </div>
          <div className='input-label'>
            <label htmlFor='password'>Password</label>
            <input type={type ? 'text' : 'password'} name='password' value={user.password} onChange={handleInput}
            onFocus={inputLabelUp} onBlur={inputLabelDown}/>
            <Eye type={type} handlePassword={(e) => setType(!type)}/>
          </div>
          {/* <select>
            <option value={0} disabled selected hidden>Choose a option</option>
            {typeOperation && 
            typeOperation.map((operation) => 
            <option key={operation.type_operation_id} value={operation.type_operation_id}>{operation.description_type_operation}</option>)}</select> */}
          <div className='btns'>
          <input type='submit' value='confirm'/>
          <button onClick={() => {navigate('/login')}}>Back</button>
          </div>
          {message && <span>invalid data</span>}
        </form>
      </div>
    </div>
  )
}
