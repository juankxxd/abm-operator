export function inputLabelUp(e) {
  const label = e.target.parentNode.firstChild;
  label.classList.add('input-up')
}

export function inputLabelDown(e) {
  const label = e.target.parentNode.firstChild;
  const input = label.nextSibling;
  if(!input.value){
    label.classList.remove('input-up')
  }
  
}
