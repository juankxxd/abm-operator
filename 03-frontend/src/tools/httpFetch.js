import { API_HOST } from '../utils/constants';
/**
 * function that makes the call to the apis of the server
 * @returns promise with information
 */
const httpFetch = () => {
  const customFetch = ({
    endpoint, optionsHeader
  }) => {
    const defaultHeader = {
      accept: 'application/json',
      'content-type': 'application/json',
    };
    const options = optionsHeader;
    const controller = new AbortController();
    options.signal = controller.signal;
    options.method = options.method || 'GET';
    options.headers = options.headers
      ? { ...defaultHeader, ...options.headers }
      : defaultHeader;
    options.body = JSON.stringify(options.body) || false;
    if (!options.body) delete options.body;
    setTimeout(() => controller.abort(), 20000);
    return fetch(`${API_HOST}${endpoint}`, options)
      .then((res) => (res.ok
        ? res.json()
        // eslint-disable-next-line prefer-promise-reject-errors
        : Promise.reject({
          err: true,
          status: res.status || '00',
          statusText: res.statusText || 'Ocurrió un error',
        })))
      .then((data) => {
        return data;
      })
      .catch((err) => err);
  };

  /**
   * fetch data from server
   * @param url endpoint to consult
   * @param parameters extra data that will go in the url as id
   * @param options other data for http requests such as the body with information
   * @returns function that does the fetch
   */
  const get = (url, parameters, options = {}) => {
    let endpoint;
    if (parameters) {
      endpoint = `${url}/${parameters}`;
    }
    return customFetch({ endpoint: endpoint || url, optionsHeader: options });
  };

  /**
   * make a post to create or make filters
   * @param url endpoint to consult
   * @param options other data for http requests such as the body with information
   * @returns function that does the fetch
   */
  const post = (url, options) => {
    const optionsHeader = options || {};
    optionsHeader.method = 'POST';
    let endpoint = url;
    return customFetch({ endpoint: endpoint || url, optionsHeader });
  };

  /**
   * function to update data
   * @param url endpoint to consult
   * @param parameters extra data that will go in the url as id
   * @param options other data for http requests such as the body with information
   * @returns function that does the fetch
   */
  const patch = (url, parameters, options = {}) => {
    const optionsHeader = options || {};
    let endpoint;
    if (parameters) {
      endpoint = `${url}/${parameters}`;
    }
    optionsHeader.method = 'PATCH';
    return customFetch({ endpoint, optionsHeader });
  };

  /**
   * function to update data
   * @param url endpoint to consult
   * @param parameters extra data that will go in the url as id
   * @param options other data for http requests such as the body with information
   * @returns function that does the fetch
   */
  const put = (url, parameters, options = {}) => {
    const optionsHeader = options || {};
    let endpoint;
    if (parameters) {
      endpoint = `${url}/${parameters}`;
    }
    optionsHeader.method = 'PUT';
    return customFetch({ endpoint, optionsHeader });
  };

  return {
    get,
    post,
    put,
    patch,
  };
};

export default httpFetch;
